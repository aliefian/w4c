<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Sampah;

class HomeController extends Controller
{
    public function index()
    {   
        
    	// // mengambil data dari table sampah
        $sampah = DB::table('sampahs')
                ->orderBy('nama_sampah')
                ->paginate(10);
        
    	// // mengirim datanya ke view home
    	return view('home',['sampah' => $sampah]);
    }

    public function tambah()
    {
        // memanggil view tambah
        return view('tambah');
    }
    
    // method untuk insert data ke table sampah
    public function store(Request $request)
    {
        // insert data ke table sampah
        DB::table('sampahs')->insert([
            'nama_sampah' => $request->nama_sampah,
            'jenis_sampah' => $request->jenis_sampah,
        ]);
        // kembali ke page awal
        return redirect('/');
    
    }

    // update data sampah
    public function update(Request $request)
    {
        // update data sampah
        DB::table('sampahs')->where('id_sampah',$request->id)->update([
            'nama_sampah' => $request->nama_sampah,
            'jenis_sampah' => $request->jenis_sampah,
        ]);
        // kembali ke page awal
        return redirect('/');
    }

    
    // method untuk hapus data sampah
    public function hapus($id)
    {
        // menghapus data sampah berdasarkan id
        DB::table('sampahs')->where('id_sampah',$id)->delete();
            
        // kembali ke page awal
        return redirect('/');
    }

    // buat cari cari
    public function carinama(Request $request)
    {
        // nangkep data yg mau dicari
        $cari = $request->sampah;

        //nyari data di tabel sampah tapi berdasarkan namanya
        $sampah = DB::table('sampahs')
        ->where('nama_sampah','like',"%".$cari."%")
        ->get();

        return view('indexnama',['sampah' => $sampah]);

    }    

    // buat cari cari
    public function carijenis(Request $request)
    {
        // nangkep data yg mau dicari
        $cari = $request->jenis;

        //nyari data di tabel sampah tapi berdasarkan jenisnya
        $sampah = DB::table('sampahs')
        ->where('jenis_sampah','like',"%".$cari."%")
        ->get();

        return view('indexjenis',['sampah' => $sampah]);
        

    }    

    function list() 
    {

        return Sampah::all();

    }


}
