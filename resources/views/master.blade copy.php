<!DOCTYPE html>
<html>
<head>
	<title>Web Engineer Challenge</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	
	
</head>
<body>
 
	<header>
		<div class="d-flex justify-content-center">
			<nav>
				<a href="/">HOME</a>
				|
				<a target="_blank" href="https://github.com/aliefian9">PROFIL</a>
			</nav>
		</div>
	</header>
	<hr/>
	<br/>
 
 
	<!-- bagian konten blog -->
	@yield('konten')
 
 
	<br/>
	<br/>
	<hr/>
	<footer>	
		<div class="d-flex justify-content-center">
			<p>&copy; <a target="_blank" href="https://github.com/aliefian9">ALIEFIAN</a> 2020</p>
		</div>
	</footer>
 
</body>
</html>